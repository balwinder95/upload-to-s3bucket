var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var async = require('async');

exports.uploadFile = function (req, res) {
	var email = req.body.email;
	var file = req.files.userFile;
	var manValues = [email];
	async.waterfall([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}], function () {

			if ((file)&& (file.name)) {
				commonfunction.uploadToS3Bucket(file, 'balwinder_images', function (fileUrl) {
					if (fileUrl == 0) {
						sendResponse.somethingWentWrongError(res);
					} else {
						saveFilePathInDb(res, email, fileUrl, function (fileUrl) {
							data = {"file_uploaded": fileUrl};
							sendResponse.sendSuccessData(data, res);
						});
					}
				});
			} else {
				sendResponse.parameterMissingError(res);
			}
		});
};

function saveFilePathInDb (res, email, fileUrl, callback) {

	var sql = 'UPDATE user_info SET image=? WHERE email=?';
	dbConnection.Query(res, sql, [fileUrl, email], function (response) {
		callback(fileUrl);
	});
}