var express = require('express');
var router = express.Router();
var uploadImage = require('./uploadImage');

/* GET test page. */
router.get('/test', function (req, res) {
  res.render('test');
});

router.post('/file_uploaded', uploadImage.uploadFile);

module.exports = router;