var sendResponse = require('./sendResponse');
var AWSSettings = require('../config/AWSSettings');
var fs = require('fs');
var AWS = require('aws-sdk');
var randomstring = require('randomstring');

exports.checkBlank = function (arr) {
  return (checkBlank(arr));
};

function checkBlank(arr) {
  var arrlength = arr.length;
  for (var i = 0; i < arrlength; i++) {
    if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
      return 1;
    }
  }
  return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

  var checkBlankData = checkBlank(manValues);

  if (checkBlankData) {
    sendResponse.parameterMissingError(res);
  }
  else {
    callback(null);
  }
}

exports.uploadToS3Bucket = function (file, folder, callback) {

  var fileName = file.name;
  var filePath = file.path;
  var fileType = file.type;
  fs.readFile (filePath, function (error, fileBuffer) {

    if (error) {
      callback(0);
    } else {
      var str = randomstring.generate(7);
      fileName = 'balwinder-' + str + '-' + file.name;
      fileName = fileName.split(' ').join('-');

      var fileUrl = 'https://' + AWSSettings.awsBucket + '.s3.amazonaws.com/' + folder + '/' + fileName;

      AWS.config.update({accessKeyId: AWSSettings.awsAccessKey, secretAccessKey: AWSSettings.awsSecretKey});
      var s3bucket = new AWS.S3();
      var params = {Bucket: AWSSettings.awsBucket, Key: folder + '/' + fileName, Body: fileBuffer, ACL: 'public-read', ContentType: fileType};
      s3bucket.putObject(params, function (err, data) {
        if (err) {
          console.log(err);
          return callback(0);
        }
        else {
          return callback(fileUrl);
        }
      });
    }
  });
}