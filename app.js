process.env.NODE_ENV = 'localDevelopment';
constant = require('./routes/constant');
dbConnection = require('./routes/dbConnection');
var express = require('express');

var port = require('./config/localDevelopment').PORT;
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var routes = require('./routes/index');
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'public')));
app.get('/test', routes);
app.use('/file_uploaded', multipartMiddleware);
app.post('/file_uploaded', routes);

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
});
http.createServer(app).listen(port, function () {
    console.log("Express server listening on port " + port);
});